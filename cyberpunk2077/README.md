# Cyberpunk 2077

![Cyberpunk 2077 Header](https://shared.fastly.steamstatic.com/store_item_assets/steam/apps/1091500/header.jpg)

> **:information: Info**
>
> I launch the game through [Heroic Game Launcher](https://heroicgameslauncher.com/).

## Lunch options

```
-skipStartScreen -modded
```

- `-skipStartScreen` skips the "Breaching..." start screen.
- `-modded` is required for mods dependent on REDmod to load when skipping the launcher.

## Controls

- Mouse Sensitivity: 3
- Cyber Engine Tweaks overlay binding: `

## Mods

> **:information: Info**
>
> All mods tested in version 2.2, **not with 2.21**!

### Essentials

| Mod                                                                                                  | Version | Ready 2.21? | Notes                                            |
| :--------------------------------------------------------------------------------------------------- | :------ | :---------- | :----------------------------------------------- |
| [Cyber Engine Tweaks](https://www.nexusmods.com/cyberpunk2077/mods/107)                              | 1.34.1  | ✅           | -                                                |
| [RED4ext](https://www.nexusmods.com/cyberpunk2077/mods/2380)                                         | 1.27.0  | ❓✅          | -                                                |
| [redscript](https://www.nexusmods.com/cyberpunk2077/mods/1511)                                       | 0.5.27  | ✅           | -                                                |
| [Smooth Movement](https://www.nexusmods.com/cyberpunk2077/mods/13471)                                | 1.1     | ✅           | `CET` variant.                                   |
| [No Intro Videos](https://www.nexusmods.com/cyberpunk2077/mods/533)                                  | 0.5     | ✅           | -                                                |
| [Native Settings UI](https://www.nexusmods.com/cyberpunk2077/mods/3518)                              | 1.96    | ✅           | -                                                |
| [Immersive First Person](https://www.nexusmods.com/cyberpunk2077/mods/2675)                          | 1.5.1   | ✅           | -                                                |
| [Informative HUD Quickhacks Memory Counter](https://www.nexusmods.com/cyberpunk2077/mods/10685)      | 0.9.2   | ❓✅          | -                                                |
| [Authentic Shift](https://www.nexusmods.com/cyberpunk2077/mods/6823)                                 | 2.12.39 | ❓✅          | -                                                |
| [Light Beams Fix](https://www.nexusmods.com/cyberpunk2077/mods/12381)                                | 0.1     | ❓❌          | -                                                |
| [Alternative Hair Material](https://www.nexusmods.com/cyberpunk2077/mods/12184)                      | 0.1     | ✅           | -                                                |
| [Environment Textures Overhaul](https://www.nexusmods.com/cyberpunk2077/mods/13372)                  | 1.1     | ✅           | `4K` variant. Rename to `####ETO_1.1_4K.archive` |
| [Cyberpunk 2077 HD Reworked Project](https://www.nexusmods.com/cyberpunk2077/mods/7652)              | 2.0     | ❓❓          | `Balanced` variant.                              |
| [Reset Attributes always available](https://www.nexusmods.com/cyberpunk2077/mods/9240)               | 1.0.0.4 | ❓✅          | -                                                |
| [Better Main Menu - Pause menu - Smaller Tabs](https://www.nexusmods.com/cyberpunk2077/mods/11227)   | 2.2     | ❓           | `BetterMainMenu-PauseMenu-SmallerTabs` variant.  |
| [Always Best Quality (Ads, Map, Photo Mode,...)](https://www.nexusmods.com/cyberpunk2077/mods/12700) | 3.2.2.2 | ✅           | -                                                |

### Testing

| Mod                                                               | Version | Ready 2.21? | Notes |
| :---------------------------------------------------------------- | :------ | :---------- | :---- |
| [Preem LUT 3](https://www.nexusmods.com/cyberpunk2077/mods/11510) | 3.0.1a  | ✅           | -     |
| [Preem Map](https://www.nexusmods.com/cyberpunk2077/mods/18269)   | 1.0.0a  | ✅           | -     |

### Removed

| Mod                                                                           | Version | Ready 2.21? | Notes                                        |
| :---------------------------------------------------------------------------- | :------ | :---------- | :------------------------------------------- |
| [Codeware](https://www.nexusmods.com/cyberpunk2077/mods/7780)                 | 1.15.0  | ✅           | -                                            |
| [ArchiveXL](https://www.nexusmods.com/cyberpunk2077/mods/4198)                | 1.20.1  | ✅           | -                                            |
| [TweakXL](https://www.nexusmods.com/cyberpunk2077/mods/4197)                  | 1.10.7  | ✅           | -                                            |
| [Alternative Eye Material](https://www.nexusmods.com/cyberpunk2077/mods/7340) | 1.4     | ❌           | Removed after 2.2 update (hidden by author). |



## Useful Links

- [PC Gaming Wiki](https://www.pcgamingwiki.com/wiki/Cyberpunk_2077)
- [ProtonDB](https://www.protondb.com/app/1091500)
- [Steam Community](https://steamcommunity.com/app/1091500)
- [Reddit (r/cyberpunkgame)](https://www.reddit.com/r/cyberpunkgame/)
- [NexusMods](https://www.nexusmods.com/cyberpunk2077/)
