## Install

> $HOME/.local/share/Steam/steamapps/common/Counter-Strike Global Offensive/csgo/cfg

**On Windows:**

> C:\Steam\steamapps\common\Counter-Strike Global Offensive\csgo\cfg

## autoexec.cfg

The `autoexec.cfg` should run automatically when you launch CSGO, but if you want to manually reload it, open the console and type:

`exec autoexec`

## warmup.cfg

To use the warmup/training config you should start an offline "Practice with bots" match in your selected map. After everything is loaded, open the console and type:

`exec warmup`

## Using launch options

To add custom launch options, you need to go to your Steam Library and then follow these steps:

1. Right-click on the Counter-Strike: Global Offensive entry to open the context menu and select **Properties**.

![properties](../img/properties.png)

2. Locate **LAUNCH OPTIONS**.

![launch options](../img/launchoptions.png)

Now, add the options you want

Example: `%command% -novid -high -nojoy -freq 144 -tickrate 128 +exec autoexec.cfg`

- You always need `%command%`. This is want runs the game. Every game option should come after `%command%`.
- Change `-freq 144` to whatever the refresh rate of your monitor is (e.g. 60Hz `-freq 60`).
- Choose between `-tickrate 128` and `-tickrate 64` depending on how many ticks you want your offline maps to be running on.
- The `-novid` option disables the intro video.
- Using the `-high` option ensures that CS:GO is started with in high-priority mode.
- `-nojoy` disables the support for a controller
- `-vulkan` since I use linux, I rather have the game running through Vulkan than OpenGL.

### My options

I run the game under Arch Linux. These are the options I use. If you want to use the same options you will need to install `mangohud` and `gamemode`.

`mangohud gamemoderun %command% -novid -high -nojoy -freq 144 -tickrate 128 -limitvsconst -forcenovsync -softparticlesdefaultoff -fullscreen -vulkan +exec autoexec.cfg`