# Launch Options

`-novid -high -nojoy -freq 144 -tickrate 128  +exec autoexec.cfg`

- Change `-freq 144` to whatever the refresh rate of your monitor is (e.g. 60Hz `-freq 60`).
- Choose between `-tickrate 128` and `-tickrate 64` depending on how many ticks you want your offline maps to be running on.
- The `-novid` option disables the intro video.
- Using the `-high` option ensures that CS:GO is started with in high-priority mode.
- `-nojoy` disables the support for a controller
- `-vulkan` since I use linux, I rather have the game running through vulkan than OpenGL.

## My options

`mangohud gamemoderun %command% -novid -high -nojoy -freq 144 -tickrate 128 -limitvsconst -forcenovsync -softparticlesdefaultoff -fullscreen -vulkan +exec autoexec.cfg`
