# :zap: Jolt Configs :video_game:

> **:exclamation: Important**
> I play on Linux, but I try to make sure the instructions are as platform-agnostic as possible.

## :thought_balloon: Motivation

During the Covid-19 lockdown I introduced a lot of new players to **Couter-Strike: Global Offensive** (:headstone: RIP). However, coming up with a `autoexec.cfg` file and customising it could be quite overwhelming. With that in mind, I decided to restructure my own config files and document them the best I could.

Today, this repository serves mainly for my own game configs and to share with my friends when they ask me about my settings.

## :video_game: Game Configs

- [Blacktail](./blacktail/)
- [Counter-Strike 2](./cs2/)
- [Cyberpunk 2077](./cyberpunk2077/)
- [Deep Rock Galactic](./deep-rock-galactic)
- [Metro 2033: Redux](./metro2033-redux/)
- [Minecraft](./minecraft/)
- [Overwatch 2](./ow2/)

## :game_die: Miscellaneous

- [Nexusmods](./misc/nexusmods.md)

## :hammer: Tools

- https://aiming.pro/mouse-sensitivity-calculator

## :file_folder: Archive

- [Couter-Strike: Global Offensive](./csgo/) (deprecated in favour of CS2)
