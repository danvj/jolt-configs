# Metro 2033 Redux

![Metro 2033 Redux Header](https://shared.fastly.steamstatic.com/store_item_assets/steam/apps/286690/header.jpg)

## Launch Options

> **:information_source: Info**
> These are launch options for Steam. You need to have [MangoHud](../misc/mangohud.md) and [GameMode](../misc/gamemode.md) installed.

`mangohud gamemoderun %command%`


## Useful Links

- [PC Gaming Wiki](https://www.pcgamingwiki.com/wiki/Metro_2033_Redux)
