# GameMode

## Config

`gamemode.ini`

```ini
[general]
reaper_freq=5
desiredgov=performance
igpu_desiredgov=powersave
igpu_power_threshold=-1
softrealtime=off
renice=0
ioprio=0
inhibit_screensaver=1

[filter]

[gpu]
apply_gpu_optimisations=1
gpu_device=0
amd_performance_level=high

[supervisor]

[custom]
start=notify-send "GameMode started"
end=notify-send "GameMode ended"
```

## Useful Links

- [Github](https://github.com/FeralInteractive/gamemode)
