# Mangohud

## Config

`mangohud.conf`

```conf
preset=3
gpu_stats
cpu_stats
fps
frametime
throttling_status
frame_timing
font_size=12
text_outline
position=top-right
round_corners= 1
offset_x=0
offset_y=100
background_alpha=0.2
alpha=0.9
```

## Useful Links

- [Github](https://github.com/flightlessmango/MangoHud)