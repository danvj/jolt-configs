# Deep Rock Galactic

![Deep Rock Galactic Header](https://shared.fastly.steamstatic.com/store_item_assets/steam/apps/548430/header.jpg)

## Config Files

Copy [Engine.ini](./Engine.ini) into `/SteamLibrary/steamapps/common/Deep Rock Galactic/FSD/Saved/Config/WindowsNoEditor/Engine.ini`. It should "Skip intro videos" and "Reduce mouse input lag".

## Lunch Options

```
gamemoderun mangohud %command% -nosplash
```

## Settings

Assumes default settings. I only documented the changes to the defaults.

### Gameplay

| Option        | Value |
| :------------ | :---- |
| Field of view | 103   |
| View bob      | 0%    |
| Weapon sway   | 15%   |
| Camera shakes | 25%   |

### Controls

| Option      | Value |
| :---------- | :---- |
| Sensitivity | 263   |

### Audio

| Option        | Value |
| :------------ | :---- |
| Master volume | 20%   |

### UI

| Option             | Value  |
| :----------------- | :----- |
| Show network stats | Simple |
| UI Scale           | 80%    |
| Chat font size     | 18     |

### Graphics

| Option                | Value |
| :-------------------- | :---- |
| Anti-Aliasing         | TAA   |
| Gamma                 | 0     |
| HDR Gamma             | 1.2   |
| Upscaling             | Off   |
| Resolution scale      | 100%  |
| Texture Quality       | Ultra |
| Shadow Quality        | High  |
| Anti-Aliasing Quality | Ultra |
| Post Processing       | High  |
| Effects               | High  |
| View Distance         | Ultra |
| Boom                  | On    |
| Lens Flare            | Off   |
| Ragdoll lifetime      | Med   |
| HDR Output            | Off   |

## Installed Mods

- To install mods, go to the Menu, select the `Modding`
- Agree with the terms of service (after reading them, of course!)
- Login to mod.io with the Steam Account.

> **:information: Info**
>
>  All mods listed were tested in Season 5.

| Mod                                                                                                    | Version  | Notes |
| :----------------------------------------------------------------------------------------------------- | :------- | :---- |
| [Mod HUB](https://mod.io/g/drg/m/mod-hub)                                                              | 1.2.5    | -     |
| [DRGLib](https://mod.io/g/drg/m/drglib)                                                                | 4.8.1    | -     |
| [Death Marker](https://mod.io/g/drg/m/death-marker)                                                    | 1.2.1.1  | -     |
| [Brighter Objects](https://mod.io/g/drg/m/brighter-objects)                                            | 1.12     | -     |
| [Brighter Objects Add-on](https://mod.io/g/drg/m/brighter-objects-add-on)                              | 2.1      | -     |
| [Fast Forging Continued](https://mod.io/g/drg/m/fast-forging-continued)                                | 6        | -     |
| [Weapon Heat Crosshair](https://mod.io/g/drg/m/weapon-heat-crosshair)                                  | 2.2.4    | -     |
| [Ammo Percentage Indicator Extended](https://mod.io/g/drg/m/ammo-percentage-indicator-e)               | nyhu     | -     |
| [Better Explosion Range Indicator](https://mod.io/g/drg/m/better-explosion-range-indicator)            | 2.3.1    | -     |
| [枪手护盾特效替换/Modified gunner shield effect](https://mod.io/g/drg/m/modified-gunner-shield-effect) | 1.0.0    | -     |
| [Combined Resources](https://mod.io/g/drg/m/combined-resources)                                        | 3.1.0    | -     |
| [Live Mission Stat Tracker +](https://mod.io/g/drg/m/live-mission-stat-tracker-plus)                   | 1.7.0    | -     |
| [Status Effect Duration Indicator](https://mod.io/g/drg/m/status-effect-duration-indicator)            | 1.7.0    | -     |
| [Better Spectator](https://mod.io/g/drg/m/better-spectator-reloaded)                                   | 4        | -     |
| [Refinery Piper QOL](https://mod.io/g/drg/m/refinery-pipe-qol)                                         | 1.4      | -     |
| [Dysacell Cargo Crate Batteries](https://mod.io/g/drg/m/dysacell)                                      | 22.05.22 | -     |
| [Pickaxe Bonk! SFX](https://mod.io/g/drg/m/pickaxe-bonk-sfx)                                           | 1.7      | -     |

## Other Mods

Mods that seem interesting but are not updated to run with the latest versions.

| Mod                                                                             | Version | Notes |
| :------------------------------------------------------------------------------ | :------ | :---- |
| [Platform Placement Preview](https://mod.io/g/drg/m/platform-placement-preview) | 1.1.1   | -     |
| [Health and Shield Numbers](https://mod.io/g/drg/m/health-and-shield-numbers)   | 1.1.1   | -     |


## Useful Links

- [PC Gaming Wiki](https://www.pcgamingwiki.com/wiki/Deep_Rock_Galactic)
- [ProtonDB](https://www.protondb.com/app/548430)
- [Steam Community](https://steamcommunity.com/app/548430)
- [Reddit (r/DeepRockGalactic/)](https://www.reddit.com/r/DeepRockGalactic/)
