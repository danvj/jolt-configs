# Overwatch 2

![Overwatch 2 Header](https://shared.fastly.steamstatic.com/store_item_assets/steam/apps/2357570/header.jpg)

> **:information: Info**
>This game stores the configurations on Blizzard's servers. What I have here is really just a reference for myself.

## Video

Field of View: 103
Gamma Correction: 2.2

## Controls

Sensitivity: 5.33%

Crosshair
![crosshair settings](crosshair-ow2.png)
