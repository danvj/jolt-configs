# Counter-Strike 2

![Counter-Strike 2 Header](https://shared.fastly.steamstatic.com/store_item_assets/steam/apps/730/header.jpg)

## Install

Copy the contents of the [cs2 folder](./cs2) and paste them into

**On Linux:**

> $HOME/.local/share/Steam/steamapps/common/Counter-Strike Global Offensive/game/csgo/cfg/

**On Windows:**

> C:\Steam\steamapps\common\Counter-Strike Global Offensive\game\csgo\cfg

## autoexec.cfg

The `autoexec.cfg` should run automatically when you launch CS2, but if you want to manually reload it, open the console and type:

`exec autoexec`

## Using launch options

To add custom launch options, you need to go to your Steam Library and then follow these steps:

1. Right-click on the Counter-Strike: Global Offensive entry to open the context menu and select **Properties**.

![properties](../img/properties.png)

2. Locate **LAUNCH OPTIONS**.

![launch options](../img/launchoptions.png)

Now, add the options you want

Example: `%command% -console -novid -high -nojoy -freq 144 -tickrate 128 +exec autoexec.cfg`

- You always need `%command%`. This is want runs the game. Every game option should come after `%command%`.
- `-console` enables the console.
- Change `-freq 144` to whatever the refresh rate of your monitor is (e.g. 60Hz `-freq 60`).
- The `-novid` option disables the intro video.
- Using the `-high` option ensures that CS:GO is started with in high-priority mode.
- `-nojoy` disables the support for a controller
- `-vulkan` since I use linux, I rather have the game running through Vulkan than OpenGL.

### My options

I run the game under Arch Linux. These are the options I use. If you want to use the same options you will need to install `mangohud` and `gamemode`.

`LD_PRELOAD="" mangohud gamemoderun %command% -console -novid -nojoy -fullscreen-windowed -vulkan -sdlaudiodriver pipewire`