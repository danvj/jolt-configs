# Blacktail

![BlacktailHeader](https://shared.fastly.steamstatic.com/store_item_assets/steam/apps/1532690/header.jpg)

## Configurations

Configuration directory (Linux): `$HOME/.local/share/Steam/steamapps/compatdata/1532690/pfx/drive_c/users/steamuser/AppData/Local/BLACKTAIL/Saved/Config/WindowsNoEditor/`

Copy the `Engine.ini` inside the configuration directory.

- FOV 100º
- Anisotropic filtering enabled
- Chromatic aberration disabled
- Motion blur disabled

**Sensitivity:** 1
**Aim-sensitivity:** 1

## Useful Links
- [Humble Bundle](https://www.humblebundle.com/store/blacktail?partner=danvj) (affiliate link)
- [PCGamingWiki](https://www.pcgamingwiki.com/wiki/Blacktail)
- [PCGamingWiki](https://www.pcgamingwiki.com/wiki/Engine:Unreal_Engine_4) (Unreal Engine 4)
- [Steam Page](https://store.steampowered.com/app/1532690/BLACKTAIL/)
